<?php

namespace Tests\AppBundle\Controller;

use Doctrine\Bundle\DoctrineBundle\Command\Proxy\DropSchemaDoctrineCommand;
use Doctrine\Bundle\DoctrineBundle\Command\Proxy\UpdateSchemaDoctrineCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpKernel\Client;
use Symfony\Component\Routing\RouterInterface;

class UserControllerTest extends WebTestCase
{
    /** @var string */
    protected $name;

    /** @var Client */
    protected $client;

    /** @var Crawler */
    protected $crawler;

    /** @var RouterInterface */
    protected $router;

    /**
     * @throws \Exception
     */
    protected function setUp()
    {
        parent::setUp();

        $this->client = static::createClient();
        $this->router = self::$kernel->getContainer()->get('router');
        $this->client->followRedirects(true);
        $this->migrateSchema();
    }

    public function tearDown()
    {
        parent::tearDown();
    }

    public function testIndex()
    {
        $this->goToUsersPage();
        $this->onUsersPage();
    }

    public function testInvalidCreate()
    {
        $this->name = uniqid();
        $this->createUser(null, null);

        die;
        $this->onCreateUserPage();
        $this->hasErrors();
    }

    public function testCreate()
    {
        $this->createUser($this->name, $this->name);
        $this->onUsersPage();
        $this->assertTrue($this->userExists());
    }

    public function testDelete()
    {
        $this->name = 'delete'.uniqid();
        $this->createUser($this->name, $this->name);
        $this->goToUsersPage();
        $this->onUsersPage();
        $this->assertTrue($this->userExists());
        $this->action('Удалить');
        $this->onUsersPage();
        $this->assertFalse($this->userExists());
    }

    public function testEdit()
    {
        $this->name = 'edit'.uniqid();
        $this->createUser($this->name, $this->name);
        $this->onUsersPage();
        $this->assertTrue($this->userExists($this->name));
        $this->action('Редактировать');
        $this->onEditUserPage();
        $this->edit('_edited', $this->name);
        $this->onUsersPage();
        $this->assertTrue($this->userExists('_edited'));
        $this->assertFalse($this->userExists($this->name));
    }

    /**
     * @throws \Exception
     */
    protected function migrateSchema()
    {
        $app = new Application(self::$kernel);
        $app->add(new UpdateSchemaDoctrineCommand());
        $arguments = [
            'command' => 'doctrine:schema:update',
            '--force' => true,
        ];

        $input = new ArrayInput($arguments);

        $command = $app->find('doctrine:schema:update');
        $output = new BufferedOutput();
        $command->run($input, $output);
    }

    /**
     * @throws \Exception
     */
    protected function clearSchema()
    {
        $app = new Application(self::$kernel);
        $app->add(new DropSchemaDoctrineCommand());
        $arguments = [
            'command' => 'doctrine:schema:drop',
            '--full-database' => true,
            '--force' => true,
        ];

        $input = new ArrayInput($arguments);

        $command = $app->find('doctrine:schema:drop');
        $output = new BufferedOutput();
        $command->run($input, $output);
    }

    protected function onUsersPage()
    {
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertEquals(1, $this->crawler->filter('#list')->count());
        $this->assertEquals(1, $this->crawler->filter('#create')->count());
    }

    protected function onCreateUserPage()
    {
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertEquals(1, $this->crawler->filter('form')->count());
    }

    protected function onEditUserPage()
    {
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
        $this->assertEquals(1, $this->crawler->filter('form')->count());
    }

    protected function goToUsersPage($page = 'users_index')
    {
        $url = $this->router->generate($page);
        $this->crawler = $this->client->request('GET', $url);
    }

    protected function edit($username, $password)
    {
        $form = $this->crawler->filter('form')->form();
        $form->setValues([
            'username' => $username,
            'password' => $password,
        ]);
        $this->crawler = $this->client->submit($form);
    }

    protected function createUser($username, $password)
    {
        $this->goToUsersPage('users_create');
        die;
        $this->onCreateUserPage();
        $this->edit($username, $password);

        return true;
    }

    protected function userExists($name = null)
    {
        $name = $name ? $name : $this->name;

        return 0 < $this->crawler->filter('td:contains("'.$name.'")')->count();
    }

    protected function action($action)
    {
        $this->assertNotEmpty($this->name);
        $link = $this->crawler->filter('td:contains("'.$this->name.'") + td > a:contains("'.$action.'")');
        $this->assertEquals(1, $link->count());
        $this->crawler = $this->client->click($link->eq(0)->link());
    }

    protected function hasErrors()
    {
        $this->assertEquals(2, $this->crawler->filter('li')->count());
    }
}
