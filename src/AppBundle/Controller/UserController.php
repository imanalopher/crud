<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use AppBundle\Repository\UserRepository;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class UserController.
 *
 * @Route("/users")
 */
class UserController extends AbstractController
{
    private $repository;
    private $paginator;

    public function __construct(UserRepository $repository, PaginatorInterface $paginator)
    {
        $this->repository = $repository;
        $this->paginator = $paginator;
    }

    /**
     * @Route(name="users_index")
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $users = $this->repository->findAll();

        $pagination = $this->paginator->paginate(
            $users,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render(':User:index.html.twig', ['users' => $pagination]);
    }

    /**
     * @Route("/create", name="users_create", methods={"GET", "POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('users_index');
        }

        return $this->render(':User:create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @param int $id
     *
     * @Route("/{id}/delete", name="users_delete", requirements={"id"="\d+"})
     *
     * @return Response
     */
    public function delete($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->find($id);

        if ($user instanceof User)
        {
            $em->remove($user);
            $em->flush();
        }

        return $this->redirectToRoute("users_index");
    }

    /**
     * @Route("/{id}/edit", name="users_edit", requirements={"id"="\d+"})
     *
     * @param int     $id
     * @param Request $request
     *
     * @return Response
     */
    public function edit($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->find($id);
        if (!$user instanceof User)
            throw new NotFoundHttpException('Not found');

        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('users_index');
        }

        return $this->render(':User:create.html.twig', ['form' => $form->createView()]);
    }
}
